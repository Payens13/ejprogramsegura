import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        GeneradorClaves generador = new GeneradorClaves();
        CifradoYFirma cifrador = new CifradoYFirma();
        DescifrarYFirma descifrador = new DescifrarYFirma();

        Scanner scanner = new Scanner(System.in);
        int opcion = 0;

        while (opcion != 4) {
            System.out.println("Selecciona una opción:");
            System.out.println("1. Generar claves");
            System.out.println("2. Cifrar y firmar mensaje");
            System.out.println("3. Descifrar mensaje y verificar firma");
            System.out.println("4. Salir");

            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    // Generar claves
                    generador.generarClaves();
                    break;
                case 2:
                    // Cifrar y firmar mensaje
                    cifrador.cifrarYFirmar();
                    break;
                case 3:
                    // Descifrar mensaje y verificar firma
                    descifrador.descifrarYVerificar();
                    break;
                case 4:
                    System.out.println("Saliendo...");
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, selecciona una opción del 1 al 4.");
                    break;
            }
        }

        scanner.close();
    }
}