import java.security.*;
import java.security.spec.*;
import javax.crypto.Cipher;
import java.io.*;
import java.nio.file.Files;

public class DescifrarYFirma {
    public void descifrarYVerificar() throws Exception {
        // Leer la clave pública de los ficheros
        byte[] keyBytes = Files.readAllBytes(new File("publicKey").toPath());
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey publicKey = kf.generatePublic(spec);

        // Leer el mensaje cifrado y la firma de los archivos
        FileInputStream fis = new FileInputStream("mensajeCifrado");
        byte[] mensajeCifrado = fis.readAllBytes();
        fis.close();

        fis = new FileInputStream("firmaMensaje");
        byte[] firma = fis.readAllBytes();
        fis.close();

        // Descifrar el mensaje
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        String mensajeDescifrado = new String(cipher.doFinal(mensajeCifrado));

        // Verificar la firma
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(publicKey);
        publicSignature.update(mensajeDescifrado.getBytes());
        boolean firmaVerificada = publicSignature.verify(firma);

        System.out.println("Mensaje descifrado: " + mensajeDescifrado);
        System.out.println("Firma verificada: " + firmaVerificada);
    }
}