import java.security.*;
import java.io.*;
import java.nio.file.*;
import java.security.spec.*;

public class GeneradorClaves {
    PublicKey publicKey;
    PrivateKey privateKey;

    public GeneradorClaves() {
        generarClaves();
    }

    public void generarClaves() {
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");

            if (Files.exists(Paths.get("privateKey")) && Files.exists(Paths.get("publicKey"))) {
                // Leer las claves de los ficheros
                byte[] keyBytes = Files.readAllBytes(new File("privateKey").toPath());
                PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
                this.privateKey = kf.generatePrivate(spec);

                keyBytes = Files.readAllBytes(new File("publicKey").toPath());
                X509EncodedKeySpec specPublic = new X509EncodedKeySpec(keyBytes);
                this.publicKey = kf.generatePublic(specPublic);
            } else {
                // Generar un nuevo par de claves
                KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
                KeyPair keyPair = keygen.genKeyPair();
                this.publicKey = keyPair.getPublic();
                this.privateKey = keyPair.getPrivate();

                // Guardar claves en archivos
                FileOutputStream fos = new FileOutputStream("privateKey");
                fos.write(privateKey.getEncoded());
                fos.close();

                fos = new FileOutputStream("publicKey");
                fos.write(publicKey.getEncoded());
                fos.close();
            }
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }
}