import java.security.*;
import java.security.spec.*;
import javax.crypto.Cipher;
import java.io.*;
import java.nio.file.Files;

public class CifradoYFirma {
    public void cifrarYFirmar() throws Exception {
        // Leer las claves de los ficheros
        byte[] keyBytes = Files.readAllBytes(new File("privateKey").toPath());
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = kf.generatePrivate(spec);

        // Leer el mensaje de la terminal
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduce el mensaje a cifrar y firmar:");
        String mensaje = reader.readLine();

        // Cifrar el mensaje
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes());

        // Firmar el mensaje
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(mensaje.getBytes());
        byte[] firma = privateSignature.sign();

        // Guardar el mensaje cifrado y la firma en archivos
        FileOutputStream fos = new FileOutputStream("mensajeCifrado");
        fos.write(mensajeCifrado);
        fos.close();

        fos = new FileOutputStream("firmaMensaje");
        fos.write(firma);
        fos.close();
    }
}